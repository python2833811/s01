# [Section] Comments
# Comments in Python are done using "ctrl+/" or # symbol

# [Section] Python Syntax
# Hello World in Python
print("Hello World!")

# [Section] Indentation
# Indentation in Python indicate a block of code

# [Section] Variables
# Variables are data containers. In Python, you can declare a variable by declaring a name and assignment value using an equal symbol
age = 35
print(age)

# [Section] Naming Convention 
# The terminology used for variable names is "identifier"
# All identifiers should begin with a letter (A-Z or a-z), dollar sign or underscore
# After the first character, identifier can have any combination of characters
# In Python, it's best practice to use snake case which is separating them via underscore "_" as defined by PEP (Python enhancement proposal)
# Identifiers are case sensitive
middle_initial = "C"

# Python allows assigning value to multiple variables in one line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name2)

# [Section] Data Types
# Data types convey what kind of information a variable holds. There are different data types and each has its own use

# In Python, there are commonly used data types:
# 1. String (str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

#2. Numbers (int, float, complex) - for integers, decimal and complex number
num_of_days = 365 #This is an integer
pi_approx = 3.141 #This is a float
complex_num = 1 + 5j #This is complex where the letter j represents imaginary number

print(type(complex_num))





